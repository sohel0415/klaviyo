<?php

namespace App\Helper;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class KlaviyoApiHelper
{
    private $apiKey;
    private $listId;

    public function __construct()
    {
        $this->apiKey = env('klaviyo_private_key');
        $this->listId = env('klaviyo_list_id');
    }

    public function getAllMembers()
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', 'https://a.klaviyo.com/api/v2/group/' . $this->listId . '/members/all?api_key=' . $this->apiKey, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        Log::info("All profiles " . $response->getBody());
    }

    public function subscribe(array $contacts): array
    {
        $client = new \GuzzleHttp\Client();
        $profiles = [];
        foreach ($contacts as $contact) {
            $profile = new \stdClass();
            foreach ($contact as $key => $value) {
                $profile->{$key} = $value;
            }
            $profiles[] = $profile;
        }

        $requestBody = new \stdClass();
        $requestBody->profiles = $profiles;

        $response = $client->request(
            'POST',
            'https://a.klaviyo.com/api/v2/list/' . $this->listId . '/members?api_key=' . $this->apiKey, [
            'body' => json_encode($requestBody),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        return json_decode($response->getBody());
    }

    public function getProfile(string $personId)
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', 'https://a.klaviyo.com/api/v1/person/' . $personId . '?api_key=' . $this->apiKey, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        Log::info("created profile " . $response->getBody());
    }

    public function updateProfile(string $personId, array $profile): void
    {
        $client = new \GuzzleHttp\Client();
        $queryString = http_build_query($profile);

        $response = $client->request('PUT', 'https://a.klaviyo.com/api/v1/person/' . $personId . '?' . $queryString . '&api_key=' . $this->apiKey, [
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);

        Log::info("updated profile " . $response->getBody());
    }

    public function unsubscribe(array $emails, array $phoneNumbers)
    {
        $client = new \GuzzleHttp\Client();

        $requestBody = new \stdClass();
        $requestBody->emails = $emails;
        $requestBody->phone_numbers = $phoneNumbers;

        $response = $client->request('DELETE', 'https://a.klaviyo.com/api/v2/list/' . $this->listId . '/subscribe?api_key=' . $this->apiKey, [
            'body' => json_encode($requestBody),
            'headers' => [
                'Content-Type' => 'application/json',
            ],
        ]);

        echo $response->getBody();
    }

    public function track()
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', 'https://a.klaviyo.com/api/track', [
            'form_params' => [
                'data' => '{"token": "' . env('klaviyo_public_key') . '", "event": "Jim orr test project button click tracking", "customer_properties": {"$email": "jim_orr@gmail.com"}, "properties": {"item_name": "Track button time","$value": ' . time() . '}}'
            ],
            'headers' => [
                'Accept' => 'text/html',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ],
        ]);

        Log::info($response->getBody());

        return json_decode($response->getBody());
    }
}