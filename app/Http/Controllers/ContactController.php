<?php

namespace App\Http\Controllers;

use App\Helper\KlaviyoApiHelper;
use App\Http\Requests\CreateContact;
use App\Http\Requests\UpdateContact;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::where('user_id', Auth::user()->id)->get();
        (new KlaviyoApiHelper())->getAllMembers();

        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateContact $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateContact $request)
    {
        $inputs = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number')
        ];
        Contact::create($inputs + ['user_id' => Auth::user()->id]);

        $responses = (new KlaviyoApiHelper())->subscribe([$inputs]);

        foreach ($responses as $response) {
            (new KlaviyoApiHelper())->getProfile($response->id);
            Contact::where('email', $response->email)->update(['klaviyo_person_id' => $response->id]);
        }

        return redirect('/contacts')->with('successMessage', 'Contact created and synced successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::findOrFail($id);

        return view('contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateContact $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContact $request, $id)
    {
        $inputs = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone_number' => $request->get('phone_number')
        ];
        $contact = Contact::find($id);
        $contact->fill($inputs)->save();

        (new KlaviyoApiHelper())->updateProfile($contact->klaviyo_person_id, $inputs);

        return redirect('contacts')->with('successMessage', 'Contact updated and synced successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        (new KlaviyoApiHelper())->unsubscribe([$contact->email], [$contact->phone_number]);
        $contact->delete();

        return redirect('contacts')->with('successMessage', 'Contact deleted and synced successfully');
    }

    public function track()
    {
        return (new KlaviyoApiHelper())->track();
    }

    public function import()
    {
        return view('contacts.import_csv');
    }

    public function postImport(Request $request)
    {
        if ($_POST) {
            $request->validate([
                'csv_file' => 'required|mimes:csv,txt'
            ]);
            $file = $request->file('csv_file');
            $name = time() . '.csv';
            $path = public_path('documents' . DIRECTORY_SEPARATOR);

            if ($file->move($path, $name)) {
                $contacts = $this->csvToArray($path . $name);

                foreach ($contacts as $contact) {
                    Contact::create($contact + ['user_id' => Auth::user()->id]);
                }

                $responses = (new KlaviyoApiHelper())->subscribe($contacts);
                foreach ($responses as $response) {
                    (new KlaviyoApiHelper())->getProfile($response->id);
                    Contact::where('email', $response->email)->update(['klaviyo_person_id' => $response->id]);
                }
            }
        }

        return redirect('/contacts')->with('successMessage', 'Contact created and synced successfully');
    }

    private function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
