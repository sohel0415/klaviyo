@props(['label', 'name', 'id', 'errors', 'required' => false, 'value' => null, 'sample' => null, 'multiple' => null, 'nameInErrors' => null])

<?php if ($multiple) {
    $nameInErrors = $nameInErrors;
} else {
    $nameInErrors = $name;
} ?>
<div class="form-group">
    <label for="{{$id}}">
        {{$label}}@if($required) <span class="color-red">*</span>@endif
        @if($sample) (<a href="{{asset($sample)}}" target="_blank">Download sample</a>)@endif
    </label>
    <input type="file" id="{{$id}}" name="{{$name}}"
           class="form-control {{$errors->first($nameInErrors) != null || $errors->first($nameInErrors . '.*') != null ? 'is-invalid' : ''}}"
           @if($multiple) multiple="multiple" @endif>
    <span class="error {{$errors->first($nameInErrors) != null || $errors->first($nameInErrors . '.*') != null ? 'invalid-feedback' : ''}}"
          style="display: inline;">{{!empty($errors->first($nameInErrors)) ? $errors->first($nameInErrors) : $errors->first($nameInErrors . '.*')}}</span>
</div>