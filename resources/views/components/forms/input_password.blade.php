@props(['label', 'name', 'id', 'errors', 'required' => false, 'value' => null])

<div class="form-group">
    <label for="{{$id}}">{{$label}} @if($required)<span class="color-red">*</span>@endif</label>
    <input type="password" id="{{$id}}" name="{{$name}}"
           class="form-control {{$errors->first($name) != null ? 'is-invalid' : ''}}">
    <span class="error {{$errors->first($name) != null ? 'invalid-feedback' : ''}}"
          style="display: inline;">{{$errors->first($name)}}</span>
</div>