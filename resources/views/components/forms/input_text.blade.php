@props(['label', 'name', 'id', 'errors', 'required' => false, 'value' => null, 'readonly' => null, 'spanInLabel'=>null])

<div class="form-group">
    <label for="{{$id}}">{{$label}} @if($spanInLabel)<span
                style='color:red;'>{{$spanInLabel}}</span>@endif @if($required)<span
                class="color-red">*</span>@endif</label>
    <input type="text" id="{{$id}}" name="{{$name}}" value="{{old($name, $value)}}"
           class="form-control {{$errors->first($name) != null ? 'is-invalid' : ''}}" @if($readonly) readonly @endif>
    <span class="error {{$errors->first($name) != null ? 'invalid-feedback' : ''}}"
          style="display: inline;">{{$errors->first($name)}}</span>
</div>