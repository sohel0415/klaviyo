@extends('layout.layout')

@section('title', 'Contact Create')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Contact Create</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <a href="{{url('/contacts')}}" class="btn btn-link float-right">
                            <i class="fas fa-backward"></i> Back to List</a>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-body">
                                <form action="{{url('contacts')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <x-forms.input_text id="name" name="name" label="Name" required="true"
                                                        :errors="$errors"/>
                                    <x-forms.input_text id="email" name="email" label="Email" required="true"
                                                        :errors="$errors"/>
                                    <x-forms.input_text id="phone_number" name="phone_number" label="Phone number"
                                                        required="true"
                                                        :errors="$errors"/>
                                    <div class="form-group">
                                        <input type="submit" value="Create" class="btn btn-success float-right">
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection