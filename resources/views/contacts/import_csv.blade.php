@extends('layout.layout')

@section('title', 'Contact Import')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Contact Mass Import</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <a href="{{url('/contacts')}}" class="btn btn-link float-right">
                            <i class="fas fa-backward"></i> Back to List</a>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default box -->
                        <div class="card">
                            <div class="card-body">
                                <form action="{{url('contacts/import')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @if ($errors->any())
                                        <div class="col-sm-12">
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-sm-6">
                                        <x-forms.input_file id="csv_file" name="csv_file" sample="sample.csv"
                                                            label="Upload csv" required="true" :errors="$errors"/>
                                        <div class="form-group">
                                            <input type="submit" value="Import"
                                                   class="btn btn-success float-right">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection