@extends('layout.layout')

@section('title', 'Contact List')

@section('headerCSSBeforeAdminLte')
    <link rel="stylesheet" href="{{asset('theme/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('theme/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Contact List</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <a href="#" class="btn btn-primary float-right track">
                            <i class="fas fa-activity"></i> Track </a>
                        <a href="{{url('contacts/create')}}"
                           class="btn btn-primary float-right" style="margin-right: 5px;">
                            <i class="fas fa-plus"></i> Create new</a>
                        <a href="{{url('contacts/import')}}"
                           class="btn btn-primary float-right"
                           style="margin-right: 5px;">
                            <i class="fas fa-upload"></i> Mass Import</a>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                @include('layout.flash_message')
                                <table id="users_list" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th class="project-actions">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contacts as $contact)
                                        <tr>
                                            <td>{{$contact->name}}</td>
                                            <td>{{$contact->email}}</td>
                                            <td>{{$contact->phone_number}}</td>
                                            <td class="project-actions text-right">
                                                <form action="{{url('contacts/'.$contact->id)}}" method="post"
                                                      onsubmit="return confirm('Do you really want to delete?');">
                                                    @csrf
                                                    @method('delete')
                                                    <a class="btn btn-info btn-sm"
                                                       href="{{url('contacts/'.$contact->id.'/edit')}}">
                                                        <i class="fas fa-pencil-alt" title="Edit"></i>
                                                    </a>

                                                    <button type="submit" class="btn btn-danger btn-sm">
                                                        <i class="fas fa-trash" title="Delete"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('footerJS')
    <script src="{{asset('theme/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('theme/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('theme/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('theme/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('theme/plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('theme/plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('theme/plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('theme/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('theme/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('theme/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
@endsection

@section('customJs')
    <script>
        $(function () {
            let userListTable = $("#users_list").DataTable({
                "pageLength": 25,
                "scrollX": true,
                "order": [],
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: ':visible:not(.project-actions)'
                        }
                    },
                    "colvis"
                ]
            });

            userListTable.buttons().container().appendTo('#users_list_wrapper .col-md-6:eq(0)');

            $(window).on('resize', function () {
                userListTable.columns.adjust().draw();
            });

            $('#toggle_push_menu').on('click', function () {
                setTimeout(function () {
                    userListTable.columns.adjust().draw();
                }, 302);
            });

            $('.track').on('click', function () {
                fetch('{{url('contacts/track')}}')
                    .then(response => response.json())
                    .then(response => console.log(response))
                    .catch(err => console.error(err));
            });
        });
    </script>
@endsection