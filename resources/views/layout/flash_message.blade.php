@if (session()->has('errorMessage'))
    <div class="alert alert-danger top-buffer">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session()->get('errorMessage') }}
    </div>
@endif
@if (session()->has('successMessage'))
    <div class="alert alert-success top-buffer">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ session()->get('successMessage') }}
    </div>
@endif