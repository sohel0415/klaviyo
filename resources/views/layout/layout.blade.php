<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{env('APP_NAME', 'Test project')}} | @yield('title')</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('theme/plugins/fontawesome-free/css/all.min.css')}}">

@yield('headerCSSBeforeAdminLte')

<!-- Theme style -->
    <link rel="stylesheet" href="{{asset('theme/dist/css/adminlte.min.css')}}">

    @yield('headerCSSAfterAdminLteCSS')
    <style>
        .brand-link {
            font-size: 1.1rem;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

@include('layout.top_navbar')
<!-- Main Sidebar Container -->
    @include('layout.sidebar')

    @yield('content')
    @include('layout.footer')

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('theme/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('theme/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

@yield('footerJS')

<!-- AdminLTE App -->
<script src="{{asset('theme/dist/js/adminlte.min.js')}}"></script>
<script>
    $('.customer-page-link').on('click', function (e) {
        localStorage.clear();

        return true;
    });
</script>
@yield('customJs')

</body>
</html>
